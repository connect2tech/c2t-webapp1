Command to start the server
---------------------------
mvn -Djetty.port=8888 jetty:run

Command to stop the server
--------------------------
Ctrl+C

URL to access the application
-----------------------------
http://localhost:8888/c2t-WebApp1